package com.summa.jcox;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleRestApplicationTests {

	@Test
	public void contextLoads() {
		Assert.assertTrue(1 == 1);
	}

	@Test
	public void otherTest() {
		Assert.assertEquals(2, 2);
	}

}
