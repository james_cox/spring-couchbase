package com.summa.jcox;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdditionControllerTest {

	@Autowired
    private MockMvc mvc;
	
	
	@Test
	public void onePlusTwo() throws Exception {
		
		mvc.perform(get("/add")
				.param("augend", "1")
				.param("addend", "2")
				.accept( MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.augend", is(1)))
        .andExpect(jsonPath("$.summand", is(2)))
        .andExpect(jsonPath("$.sum", is(3)))
        .andExpect(jsonPath("$.calculatedAt", notNullValue()));
        
	}
	
}
