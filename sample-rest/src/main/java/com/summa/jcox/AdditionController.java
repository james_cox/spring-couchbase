package com.summa.jcox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdditionController {

	private static final Logger log = LoggerFactory.getLogger(AdditionController.class);

	@Autowired
	AdditionService additionService;
	
	@RequestMapping(path="/add", method=RequestMethod.GET)
	public ResponseEntity<Answer> add(@RequestParam("augend") Integer augend, @RequestParam("addend") Integer addend) {
		
		log.debug("AdditionController request processing {} plus {}", augend, addend);
		
		Answer answer = additionService.add(augend, addend);
		
		return new ResponseEntity<Answer> ( answer, HttpStatus.OK);
	}
	
}
