package com.summa.jcox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class AdditionService {

	
	private static final Logger log = LoggerFactory.getLogger(AdditionService.class);
	
	@Cacheable("default")
	public Answer add(Integer augend, Integer addend) {
	
		log.debug("Calculating {} plus {}", augend, addend);

		return new Answer(augend, addend, augend + addend);
	}
}
