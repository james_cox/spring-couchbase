package com.summa.jcox;

import java.io.Serializable;
import java.util.Date;

public class Answer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Integer augend;
	private final Integer summand;
	private final Integer sum;
	private final Date calculatedAt = new Date();
	
	public Answer(Integer augend, Integer summand, Integer sum) {
		super();
		this.augend = augend;
		this.summand = summand;
		this.sum = sum;
	}


	public Integer getAugend() {
		return augend;
	}


	public Integer getSummand() {
		return summand;
	}


	public Integer getSum() {
		return sum;
	}


	public Date getCalculatedAt() {
		return calculatedAt;
	}
	
}
